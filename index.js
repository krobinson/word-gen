const _ = require('lodash');

_.range(20).forEach(() => {
	console.log(buildWord());	
});

function buildWord() {
	return syb_start() + vowel() + syb_end();
}

// starting consonant of a syllable
function syb_start() {
	return _.sample('bcdfghjklmnprstvw'
		.split('')
		.concat([
			'ch',
			'sh',
		]));
}

// ending consonant of a syllable
function syb_end() {
	return _.sample('bdglmnprstw'
		.split('')
		.concat([
			'ch',
			'ck',
			'st',
			'rd',
			'sh',
			'le',
			'ng'
		]));
}

function consonant() {
	return _.sample('bcdfghjklmnprstvw'.split(''));
}

function vowel() {
	return _.sample(['a', 'e', 'i', 'o', 'u']);
}